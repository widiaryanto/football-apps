/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.aryanto.widi.footballapps;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.aryanto.widi.footballapps";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 7;
  public static final String VERSION_NAME = "1.6";
  // Fields from default config.
  public static final String DETAIL_MATCH = "https://www.thesportsdb.com/api/v1/json/1/lookupevent.php?id=";
  public static final String DETAIL_PLAYER = "https://www.thesportsdb.com/api/v1/json/1/lookupplayer.php?id=";
  public static final String NEXT_MATCH = "https://www.thesportsdb.com/api/v1/json/1/eventsnextleague.php?id=";
  public static final String PAST_MATCH = "https://www.thesportsdb.com/api/v1/json/1/eventspastleague.php?id=";
  public static final String PLAYER_URL = "https://www.thesportsdb.com/api/v1/json/1/lookup_all_players.php?id=";
  public static final String SEARCH_MATCH = "https://www.thesportsdb.com/api/v1/json/1/searchevents.php?e=";
  public static final String SEARCH_TEAM = "https://www.thesportsdb.com/api/v1/json/1/searchteams.php?t=";
  public static final String TEAM_ALL = "https://www.thesportsdb.com/api/v1/json/1/search_all_teams.php?l=";
  public static final String TEAM_URL = "https://www.thesportsdb.com/api/v1/json/1/lookupteam.php?id=";
}
