package com.aryanto.widi.footballapps

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.Espresso.pressBack
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.RootMatchers.withDecorView
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import android.widget.AutoCompleteTextView
import com.aryanto.widi.footballapps.R.id.*
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import android.support.test.espresso.UiController
import android.support.test.espresso.ViewAction
import android.view.View
import org.hamcrest.Matcher
import org.jetbrains.anko.find


@RunWith(AndroidJUnit4::class)
class MainActivityTest {
    @Rule
    @JvmField var activityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun testAppBehaviour() {
        onView(withId(rv_next)).check(matches(isDisplayed()))
        delay(5)
        onView(withId(rv_next)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(5))
        onView(withId(rv_next)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(5, click()))
        onView(withId(add_to_favorite)).check(matches(isDisplayed()))
        delay(3)
        onView(withId(add_to_favorite)).perform(click())
        onView(withText("Added to favorite")).inRoot(withDecorView(not(`is`(activityRule.activity.window.decorView)))).check(matches(isDisplayed()))
        delay(5)

        pressBack()
        onView(withId(spinner_next)).check(matches(isDisplayed()))
        onView(withId(spinner_next)).perform(click())
        onView(withText("German Bundesliga")).perform(click())
        delay(5)
        onView(withId(tab_match)).check(matches(isDisplayed()))
        onView(withId(viewpager_match)).perform(swipeLeft())
        delay(3)
        onView(withId(rv_past)).check(matches(isDisplayed()))
        onView(withId(rv_past)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(10))
        onView(withId(rv_past)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(10, click()))
        onView(withId(add_to_favorite)).check(matches(isDisplayed()))
        delay(3)
        onView(withId(add_to_favorite)).perform(click())
        onView(withText("Added to favorite")).inRoot(withDecorView(not(`is`(activityRule.activity.window.decorView)))).check(matches(isDisplayed()))
        delay(5)

        pressBack()
        onView(withId(spinner_past)).check(matches(isDisplayed()))
        onView(withId(spinner_past)).perform(click())
        onView(withText("French Ligue 1")).perform(click())
        delay(5)
        onView(withId(search)).check(matches(isDisplayed()))
        onView(withId(search)).perform(click())
        onView(isAssignableFrom(AutoCompleteTextView::class.java)).perform(typeText("barcelona"))
        onView(withId(rv_match)).check(matches(isDisplayed()))
        delay(5)
        onView(withId(rv_match)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(4))
        onView(withId(rv_match)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(4, click()))
        delay(3)

        pressBack()
        pressBack()
        pressBack()
        delay(3)
        onView(withId(refresh_match)).perform(withCustomConstraints(swipeDown(), isDisplayingAtLeast(80)))
        delay(3)

        onView(withId(bottom_navigation)).check(matches(isDisplayed()))
        onView(withId(teams)).perform(click())
        delay(5)
        onView(withId(rv_team)).check(matches(isDisplayed()))
        onView(withId(rv_team)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(2))
        onView(withId(rv_team)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(2, click()))
        onView(withId(layout_overview)).check(matches(isDisplayed()))
        delay(5)
        onView(withId(viewpager_team)).perform(withCustomConstraints(swipeLeft(), isDisplayed()))
        delay(5)
        onView(withId(rv_player)).check(matches(isDisplayed()))
        onView(withId(rv_player)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(1))
        onView(withId(rv_player)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))
        delay(5)
        onView(withId(layout_player)).check(matches(isDisplayed()))

        pressBack()
        onView(withId(add_to_favorite)).check(matches(isDisplayed()))
        delay(3)
        onView(withId(add_to_favorite)).perform(click())
        onView(withText("Added to favorite")).inRoot(withDecorView(not(`is`(activityRule.activity.window.decorView)))).check(matches(isDisplayed()))
        delay(5)

        pressBack()
        onView(withId(spinner_team)).check(matches(isDisplayed()))
        onView(withId(spinner_team)).perform(click())
        onView(withText("English League Championship")).perform(click())
        delay(5)
        onView(withId(search)).check(matches(isDisplayed()))
        onView(withId(search)).perform(click())
        onView(isAssignableFrom(AutoCompleteTextView::class.java)).perform(typeText("arsenal"))
        onView(withId(rv_team)).check(matches(isDisplayed()))
        delay(5)
        onView(withId(rv_team)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(0))
        onView(withId(rv_team)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        onView(withId(add_to_favorite)).check(matches(isDisplayed()))
        delay(3)
        onView(withId(add_to_favorite)).perform(click())
        onView(withText("Added to favorite")).inRoot(withDecorView(not(`is`(activityRule.activity.window.decorView)))).check(matches(isDisplayed()))
        delay(5)

        pressBack()
        pressBack()
        pressBack()
        delay(3)
        onView(withId(refresh_team)).perform(withCustomConstraints(swipeDown(), isDisplayingAtLeast(80)))
        delay(3)
        onView(withId(favorites)).perform(click())
        onView(withId(rv_match_favorite)).check(matches(isDisplayed()))
        delay(3)
        onView(withId(rv_match_favorite)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(0))
        onView(withId(rv_match_favorite)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        onView(withId(add_to_favorite)).check(matches(isDisplayed()))
        delay(3)
        onView(withId(add_to_favorite)).perform(click())
        onView(withText("Removed from favorite")).inRoot(withDecorView(not(`is`(activityRule.activity.window.decorView)))).check(matches(isDisplayed()))
        delay(5)

        pressBack()
        onView(withId(sr_match_favorite)).perform(withCustomConstraints(swipeDown(), isDisplayingAtLeast(80)))
        delay(3)
        onView(withId(viewpager_favorite)).perform(swipeLeft())
        delay(3)
        onView(withId(rv_team_favorite)).check(matches(isDisplayed()))
        onView(withId(rv_team_favorite)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(0))
        onView(withId(rv_team_favorite)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        onView(withId(add_to_favorite)).check(matches(isDisplayed()))
        delay(3)
        onView(withId(add_to_favorite)).perform(click())
        onView(withText("Removed from favorite")).inRoot(withDecorView(not(`is`(activityRule.activity.window.decorView)))).check(matches(isDisplayed()))
        delay(5)

        pressBack()
        onView(withId(sr_team_favorite)).perform(withCustomConstraints(swipeDown(), isDisplayingAtLeast(80)))
        delay(3)
        onView(withId(matches)).perform(click())
        delay(5)
        onView(withId(rv_next)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(2))
        onView(withId(rv_next)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(2, clickChildViewWithId(reminder)))
    }

    private fun delay(second: Long = 1){
        Thread.sleep(1000 * second)
    }

    private fun withCustomConstraints(action: ViewAction, constraints: Matcher<View>): ViewAction {
        return object : ViewAction {
            override fun getConstraints(): Matcher<View> {
                return constraints
            }

            override fun getDescription(): String {
                return action.description
            }

            override fun perform(uiController: UiController, view: View) {
                action.perform(uiController, view)
            }
        }
    }

    private fun clickChildViewWithId(id: Int): ViewAction {
        return object : ViewAction {
            override fun getConstraints(): Matcher<View>? {
                return null
            }

            override fun getDescription(): String? {
                return null
            }

            override fun perform(uiController: UiController, view: View) {
                val click = view.find(id) as View
                click.performClick()
            }
        }
    }
}