package com.aryanto.widi.footballapps.api

import com.aryanto.widi.footballapps.BuildConfig
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify

class ApiRepositoryTest {

    @Test
    fun doRequestPast() {
        val apiRepository = mock(ApiRepository::class.java)
        val url = BuildConfig.PAST_MATCH
        apiRepository.doRequest(url)
        verify(apiRepository).doRequest(url)
    }
    @Test
    fun doRequestNext() {
        val apiRepository = mock(ApiRepository::class.java)
        val url = BuildConfig.NEXT_MATCH
        apiRepository.doRequest(url)
        verify(apiRepository).doRequest(url)
    }

    @Test
    fun loadTeam() {
        val apiRepository = mock(ApiRepository::class.java)
        val url = BuildConfig.TEAM_URL
        apiRepository.doRequest(url)
        verify(apiRepository).doRequest(url)
    }

    @Test
    fun loadDetail() {
        val apiRepository = mock(ApiRepository::class.java)
        val url = BuildConfig.DETAIL_MATCH
        apiRepository.doRequest(url)
        verify(apiRepository).doRequest(url)
    }
}