package com.aryanto.widi.footballapps.presenter.search

import com.aryanto.widi.footballapps.api.ApiRepository
import com.aryanto.widi.footballapps.api.TheSportDBApi
import com.aryanto.widi.footballapps.model.MatchItem
import com.aryanto.widi.footballapps.model.view.MatchView
import com.aryanto.widi.footballapps.response.SearchMatchResponse
import com.aryanto.widi.footballapps.test.TestContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Test
import org.junit.Before
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class MatchSearchPresenterTest {

    @Mock
    private lateinit var apiRepository: ApiRepository

    @Mock
    private lateinit var view: MatchView

    @Mock
    private lateinit var gson: Gson

    private lateinit var matchSearchPresenter: MatchSearchPresenter

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        matchSearchPresenter = MatchSearchPresenter(view, apiRepository, gson, TestContextProvider())
    }

    @Test
    fun getMatchList() {
        val matchSearch: MutableList<MatchItem> = mutableListOf()
        val response = SearchMatchResponse(matchSearch)
        val input = "barcelona"

        GlobalScope.launch {
            `when`(gson.fromJson(apiRepository.doRequest(TheSportDBApi.getMatchSearch(input)).await(),
                    SearchMatchResponse::class.java)).thenReturn(response)

            matchSearchPresenter.getMatchList(input)

            verify(view).showLoading()
            verify(view).showMatch(matchSearch)
            verify(view).hideLoading()
        }
    }
}