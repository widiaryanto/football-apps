package com.aryanto.widi.footballapps.presenter

import com.aryanto.widi.footballapps.api.ApiRepository
import com.aryanto.widi.footballapps.api.TheSportDBApi
import com.aryanto.widi.footballapps.model.TeamItem
import com.aryanto.widi.footballapps.model.view.TeamView
import com.aryanto.widi.footballapps.response.TeamResponse
import com.aryanto.widi.footballapps.test.TestContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Test
import org.junit.Before
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class TeamsPresenterTest {

    @Mock
    private lateinit var apiRepository: ApiRepository

    @Mock
    private lateinit var view: TeamView

    @Mock
    private lateinit var gson: Gson

    private lateinit var teamsPresenter: TeamsPresenter

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        teamsPresenter = TeamsPresenter(view, apiRepository, gson, TestContextProvider())
    }

    @Test
    fun getTeamList() {
        val teamsItem: MutableList<TeamItem> = mutableListOf()
        val response = TeamResponse(teamsItem)
        val league = "English Premier League"

        GlobalScope.launch {
            `when`(gson.fromJson(apiRepository.doRequest(TheSportDBApi.getTeam(league)).await(),
                    TeamResponse::class.java)).thenReturn(response)

            teamsPresenter.getTeamList(league)

            verify(view).showLoading()
            verify(view).showTeamList(teamsItem)
            verify(view).hideLoading()
        }
    }
}