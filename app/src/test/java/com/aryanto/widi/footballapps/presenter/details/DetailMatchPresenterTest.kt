package com.aryanto.widi.footballapps.presenter.details

import com.aryanto.widi.footballapps.api.ApiRepository
import com.aryanto.widi.footballapps.api.TheSportDBApi
import com.aryanto.widi.footballapps.model.DetailMatchItem
import com.aryanto.widi.footballapps.model.TeamItem
import com.aryanto.widi.footballapps.model.view.details.DetailMatchView
import com.aryanto.widi.footballapps.response.TeamResponse
import com.aryanto.widi.footballapps.response.detail.DetailMatchResponse
import com.aryanto.widi.footballapps.test.TestContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Test
import org.junit.Before
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class DetailMatchPresenterTest {

    @Mock
    private lateinit var apiRepository: ApiRepository

    @Mock
    private lateinit var view: DetailMatchView

    @Mock
    private lateinit var gson: Gson

    private lateinit var detailMatchPresenter: DetailMatchPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        detailMatchPresenter = DetailMatchPresenter(view, apiRepository, gson, TestContextProvider())
    }

    @Test
    fun loadDetailMatch() {
        val team: MutableList<TeamItem> = mutableListOf()
        val detail: MutableList<DetailMatchItem> = mutableListOf()
        val responseDetail = DetailMatchResponse(detail)
        val responseTeam = TeamResponse(team)
        val matchId = "576504"
        val homeId = "133637"
        val awayId = "133604"

        GlobalScope.launch {
            `when`(gson.fromJson(apiRepository.doRequest(TheSportDBApi.getTeam(homeId)).await(),
                    TeamResponse::class.java)).thenReturn(responseTeam)
            `when`(gson.fromJson(apiRepository.doRequest(TheSportDBApi.getTeam(awayId)).await(),
                    TeamResponse::class.java)).thenReturn(responseTeam)
            `when`(gson.fromJson(apiRepository.doRequest(TheSportDBApi.getDetail(matchId)).await(),
                    DetailMatchResponse::class.java)).thenReturn(responseDetail)

            detailMatchPresenter.loadDetailMatch(homeId, awayId, matchId)

            verify(view).showLoading()
            verify(view).showDetail(team, team, detail)
            verify(view).hideLoading()
        }
    }
}