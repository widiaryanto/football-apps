package com.aryanto.widi.footballapps.presenter.details

import com.aryanto.widi.footballapps.api.ApiRepository
import com.aryanto.widi.footballapps.api.TheSportDBApi
import com.aryanto.widi.footballapps.model.TeamItem
import com.aryanto.widi.footballapps.model.view.details.DetailTeamView
import com.aryanto.widi.footballapps.response.TeamResponse
import com.aryanto.widi.footballapps.test.TestContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Test
import org.junit.Before
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class DetailTeamPresenterTest {

    @Mock
    private lateinit var apiRepository: ApiRepository

    @Mock
    private lateinit var view: DetailTeamView

    @Mock
    private lateinit var gson: Gson

    private lateinit var detailTeamPresenter: DetailTeamPresenter

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        detailTeamPresenter = DetailTeamPresenter(view, apiRepository, gson, TestContextProvider())
    }

    @Test
    fun getDetailTeam() {
        val team: MutableList<TeamItem> = mutableListOf()
        val response = TeamResponse(team)
        val teamId = "133739"

        GlobalScope.launch {
            `when`(gson.fromJson(apiRepository.doRequest(TheSportDBApi.getTeam(teamId)).await(),
                    TeamResponse::class.java)).thenReturn(response)

            detailTeamPresenter.getDetailTeam(teamId)

            verify(view).showLoading()
            verify(view).showDetailTeam(team)
            verify(view).hideLoading()
        }
    }
}