package com.aryanto.widi.footballapps.presenter

import com.aryanto.widi.footballapps.api.ApiRepository
import com.aryanto.widi.footballapps.api.TheSportDBApi
import com.aryanto.widi.footballapps.model.MatchItem
import com.aryanto.widi.footballapps.model.view.MatchView
import com.aryanto.widi.footballapps.response.MatchResponse
import com.aryanto.widi.footballapps.test.TestContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Test
import org.junit.Before
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class MatchPresenterTest {

    @Mock
    private lateinit var apiRepository: ApiRepository

    @Mock
    private lateinit var view: MatchView

    @Mock
    private lateinit var gson: Gson

    private lateinit var matchPresenter: MatchPresenter

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        matchPresenter = MatchPresenter(view, apiRepository, gson, TestContextProvider())
    }

    @Test
    fun getPastMatch() {
        val matchItem: MutableList<MatchItem> = mutableListOf()
        val response = MatchResponse(matchItem)
        val leagueId = "4328"

        GlobalScope.launch {
            `when`(gson.fromJson(apiRepository.doRequest(TheSportDBApi.getPastMatch(leagueId)).await(),
                    MatchResponse::class.java)).thenReturn(response)

            matchPresenter.getPastMatch(leagueId)

            verify(view).showLoading()
            verify(view).showMatch(matchItem)
            verify(view).hideLoading()
        }
    }
}