package com.aryanto.widi.footballapps.presenter.search

import com.aryanto.widi.footballapps.api.ApiRepository
import com.aryanto.widi.footballapps.api.TheSportDBApi
import com.aryanto.widi.footballapps.model.TeamItem
import com.aryanto.widi.footballapps.model.view.TeamView
import com.aryanto.widi.footballapps.response.TeamResponse
import com.aryanto.widi.footballapps.test.TestContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class TeamSearchPresenterTest {

    @Mock
    private lateinit var apiRepository: ApiRepository

    @Mock
    private lateinit var view: TeamView

    @Mock
    private lateinit var gson: Gson

    private lateinit var teamSearchPresenter: TeamSearchPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        teamSearchPresenter = TeamSearchPresenter(view, apiRepository, gson, TestContextProvider())
    }

    @Test
    fun getTeamList() {
        val teamItem: MutableList<TeamItem> = mutableListOf()
        val response = TeamResponse(teamItem)
        val input = "barcelona"

        GlobalScope.launch {
            `when`(gson.fromJson(apiRepository.doRequest(TheSportDBApi.getTeamSearch(input)).await(),
                    TeamResponse::class.java)).thenReturn(response)

            teamSearchPresenter.getTeamList(input)

            verify(view).showLoading()
            verify(view).showTeamList(teamItem)
            verify(view).hideLoading()
        }
    }
}