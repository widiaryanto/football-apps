package com.aryanto.widi.footballapps.presenter

import com.aryanto.widi.footballapps.api.ApiRepository
import com.aryanto.widi.footballapps.api.TheSportDBApi
import com.aryanto.widi.footballapps.model.PlayerItem
import com.aryanto.widi.footballapps.model.view.PlayerView
import com.aryanto.widi.footballapps.response.detail.DetailPlayerResponse
import com.aryanto.widi.footballapps.test.TestContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Test
import org.junit.Before
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class PlayerPresenterTest {

    @Mock
    private lateinit var apiRepository: ApiRepository

    @Mock
    private lateinit var view: PlayerView

    @Mock
    private lateinit var gson: Gson

    private lateinit var playerPresenter: PlayerPresenter

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        playerPresenter = PlayerPresenter(view, apiRepository, gson, TestContextProvider())
    }

    @Test
    fun getDetailPlayer() {
        val playerItem: MutableList<PlayerItem> = mutableListOf()
        val response = DetailPlayerResponse(playerItem)
        val playerId = "34146695"

        GlobalScope.launch {
            `when`(gson.fromJson(apiRepository.doRequest(TheSportDBApi.getDetailPlayer(playerId)).await(),
                    DetailPlayerResponse::class.java)).thenReturn(response)

            playerPresenter.getDetailPlayer(playerId)

            verify(view).showLoading()
            verify(view).showPlayer(playerItem)
            verify(view).hideLoading()
        }
    }
}