package com.aryanto.widi.footballapps.detail

import android.database.sqlite.SQLiteConstraintException
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.CollapsingToolbarLayout
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.widget.ImageView
import android.widget.TextView
import com.aryanto.widi.footballapps.api.ApiRepository
import com.aryanto.widi.footballapps.fragment.detail.DetailTeamOverview
import com.aryanto.widi.footballapps.fragment.detail.DetailTeamPlayer
import com.aryanto.widi.footballapps.model.TeamItem
import com.aryanto.widi.footballapps.model.view.details.DetailTeamView
import com.aryanto.widi.footballapps.presenter.details.DetailTeamPresenter
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import org.jetbrains.anko.find
import android.support.v4.app.FragmentPagerAdapter
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import com.aryanto.widi.footballapps.R
import com.aryanto.widi.footballapps.R.drawable.ic_add_to_favorites
import com.aryanto.widi.footballapps.R.drawable.ic_added_to_favorites
import com.aryanto.widi.footballapps.R.id.add_to_favorite
import com.aryanto.widi.footballapps.helper.database
import com.aryanto.widi.footballapps.model.favorite.FavoriteTeamItem
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.ads.MobileAds
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.toast


class DetailTeamActivity : AppCompatActivity(), DetailTeamView {

    private lateinit var viewPager: ViewPager
    private lateinit var tabLayout: TabLayout
    private lateinit var teamBadge: ImageView
    private lateinit var teamName: TextView
    private lateinit var teamFormedYear: TextView
    private lateinit var teamStadium: TextView
    private lateinit var presenter: DetailTeamPresenter
    private lateinit var pbDetailTeam: ProgressBar
    private lateinit var teams: TeamItem
    private lateinit var id: String
    private lateinit var collapsing: CollapsingToolbarLayout

    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_team)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Team Detail"
        supportActionBar?.elevation = 0f

        collapsing = find(R.id.toolbar_layout)
        collapsing.setExpandedTitleColor(ContextCompat.getColor(baseContext, android.R.color.transparent))

        pbDetailTeam = find(R.id.pb_detail_team)
        viewPager = find(R.id.viewpager_team)
        val pagerAdapter = SliderAdapter(supportFragmentManager)
        viewPager.adapter = pagerAdapter

        tabLayout = find(R.id.tab_team)
        tabLayout.post { tabLayout.setupWithViewPager(viewPager) }

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

        teamBadge = find(R.id.image_team)
        teamName = find(R.id.name_team)
        teamFormedYear = find(R.id.formed_year)
        teamStadium = find(R.id.stadium_team)

        val intent = intent
        id = intent.getStringExtra("id")

        favoriteState()
        val request = ApiRepository()
        val gson = Gson()
        presenter = DetailTeamPresenter(this, request, gson)
        presenter.getDetailTeam(id)
    }

    override fun showLoading() {
        pbDetailTeam.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        pbDetailTeam.visibility = View.INVISIBLE
    }

    override fun showDetailTeam(data: List<TeamItem>) {
        teams = TeamItem(data[0].teamId,
                data[0].teamName,
                data[0].teamBadge)
        Picasso.get().load(data[0].teamBadge).into(teamBadge)
        teamName.text = data[0].teamName
        teamFormedYear.text = data[0].teamFormedYear
        teamStadium.text = data[0].teamStadium
    }

    inner class SliderAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        private var overview = "OVERVIEW"
        private var player = "PLAYER"
        private val tabs = arrayOf(overview, player)

        override fun getItem(position: Int): Fragment? {
            when (position) {
                0 -> return DetailTeamOverview.newInstance(id)
                1 -> return DetailTeamPlayer.newInstance(id)
            }
            return null
        }

        override fun getCount(): Int {
            return tabs.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return tabs[position]
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.detail_menu, menu)
        menuItem = menu
        setFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when(item?.itemId){
            android.R.id.home -> {
                finish()
                true
            }
            add_to_favorite -> {
                if (isFavorite) removeFromFavorite() else addToFavorite()
                isFavorite =! isFavorite
                setFavorite()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun addToFavorite(){
        try {
            database.use {
                insert(FavoriteTeamItem.TABLE_TEAM_FAVORITE,
                        FavoriteTeamItem.TEAM_ID to teams.teamId,
                        FavoriteTeamItem.TEAM_NAME to teams.teamName,
                        FavoriteTeamItem.TEAM_BADGE to teams.teamBadge)
            }
            toast("Added to favorite")
        } catch (e: SQLiteConstraintException) {
            toast(e.localizedMessage)
        }
    }

    private fun removeFromFavorite() {
        try {
            database.use {
                delete(FavoriteTeamItem.TABLE_TEAM_FAVORITE, "(TEAM_ID = {id})",
                        "id" to id)
            }
            toast("Removed from favorite")
        } catch (e: SQLiteConstraintException) {
            toast(e.localizedMessage)
        }
    }

    private fun setFavorite(){
        if (isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_added_to_favorites)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_add_to_favorites)
    }

    private fun favoriteState(){
        database.use {
            val query = select(FavoriteTeamItem.TABLE_TEAM_FAVORITE)
                    .whereArgs("(TEAM_ID = {id})", "id" to id)
            val result = query.parseList(classParser<FavoriteTeamItem>())
            if (!result.isEmpty()) isFavorite = true
        }
    }
}
