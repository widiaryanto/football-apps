package com.aryanto.widi.footballapps.fragment.favorite


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aryanto.widi.footballapps.detail.DetailTeamActivity

import com.aryanto.widi.footballapps.R.color.colorAccent
import com.aryanto.widi.footballapps.R.id.rv_team_favorite
import com.aryanto.widi.footballapps.R.id.sr_team_favorite
import com.aryanto.widi.footballapps.adapter.favorite.FavoriteTeamAdapter
import com.aryanto.widi.footballapps.helper.database
import com.aryanto.widi.footballapps.model.favorite.FavoriteTeamItem
import com.aryanto.widi.footballapps.model.view.FavoriteView
import org.jetbrains.anko.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout

/**
 * A simple [Fragment] subclass.
 *
 */

class TeamFavorite : Fragment(), AnkoComponent<Context>, FavoriteView {
    private var favorite: MutableList<FavoriteTeamItem> = mutableListOf()

    private lateinit var adapter: FavoriteTeamAdapter
    private lateinit var listEvent: RecyclerView
    private lateinit var swipeRefresh: SwipeRefreshLayout

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        adapter = FavoriteTeamAdapter(favorite){
            context?.startActivity<DetailTeamActivity>("id" to "${it.teamId}")
        }
        listEvent.adapter = adapter
        showFavorite()

        swipeRefresh.onRefresh {
            favorite.clear()
            showFavorite()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return createView(AnkoContext.create(requireContext()))
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui){
        linearLayout {
            lparams(width = matchParent, height = wrapContent)
            topPadding = dip(16)
            leftPadding = dip(16)
            rightPadding = dip(16)

            swipeRefresh = swipeRefreshLayout {
                id = sr_team_favorite
                setColorSchemeResources(colorAccent,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light)

                listEvent = recyclerView {
                    id = rv_team_favorite
                    lparams(width = matchParent, height = wrapContent)
                    layoutManager = LinearLayoutManager(ctx)
                }
            }
        }
    }

    override fun showFavorite() {
        context?.database?.use {
            swipeRefresh.isRefreshing = false
            val query = select(FavoriteTeamItem.TABLE_TEAM_FAVORITE)
            val result = query.parseList(classParser<FavoriteTeamItem>())
            favorite.addAll(result)
            adapter.notifyDataSetChanged()
        }
    }
}
