package com.aryanto.widi.footballapps.model.view

import com.aryanto.widi.footballapps.model.PlayerItem

interface PlayerView {
    fun showLoading()
    fun hideLoading()
    fun showPlayer(data: List<PlayerItem>?)
}