package com.aryanto.widi.footballapps.model.view

import com.aryanto.widi.footballapps.model.TeamItem

interface TeamView {
    fun showLoading()
    fun hideLoading()
    fun showTeamList(data: List<TeamItem>)
}