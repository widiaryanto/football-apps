package com.aryanto.widi.footballapps.presenter.details

import com.aryanto.widi.footballapps.api.ApiRepository
import com.aryanto.widi.footballapps.model.view.details.DetailMatchView
import com.aryanto.widi.footballapps.response.detail.DetailMatchResponse
import com.aryanto.widi.footballapps.response.TeamResponse
import com.aryanto.widi.footballapps.test.CoroutinesContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class DetailMatchPresenter (
        private val view: DetailMatchView,
        private val apiRepository: ApiRepository,
        private val gson: Gson,
        private val context: CoroutinesContextProvider = CoroutinesContextProvider()){

    fun loadDetailMatch(homeId: String?, awayId: String?, matchId: String?){
        view.showLoading()

        GlobalScope.launch(context.main){

            val home = gson.fromJson(apiRepository.loadTeam(homeId).await(), TeamResponse::class.java)

            val away = gson.fromJson(apiRepository.loadTeam(awayId).await(), TeamResponse::class.java)

            val detailMatch = gson.fromJson(apiRepository.loadDetail(matchId).await(), DetailMatchResponse::class.java)

            view.showDetail(home.teams, away.teams, detailMatch.detailMatch)
            view.hideLoading()
        }
    }
}