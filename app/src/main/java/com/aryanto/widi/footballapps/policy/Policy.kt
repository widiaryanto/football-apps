package com.aryanto.widi.footballapps.policy

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import com.aryanto.widi.footballapps.R

class Policy : AppCompatActivity() {

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_policy)

        val myWebView: WebView = findViewById(R.id.policy_webview)
        myWebView.loadUrl("https://jadwaliga.com/#policy")
        myWebView.settings.javaScriptEnabled = true
    }
}
