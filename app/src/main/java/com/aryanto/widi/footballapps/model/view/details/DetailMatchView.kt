package com.aryanto.widi.footballapps.model.view.details

import com.aryanto.widi.footballapps.model.DetailMatchItem
import com.aryanto.widi.footballapps.model.TeamItem

interface DetailMatchView {
    fun showLoading()
    fun hideLoading()
    fun showDetail(home: List<TeamItem>?,
                   away: List<TeamItem>?,
                   detail: List<DetailMatchItem>?)
    fun addToFavorite()
    fun removeFromFavorite()
    fun favoriteState()
    fun setFavorite()
}