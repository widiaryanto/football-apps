package com.aryanto.widi.footballapps.presenter.search

import com.aryanto.widi.footballapps.api.ApiRepository
import com.aryanto.widi.footballapps.api.TheSportDBApi
import com.aryanto.widi.footballapps.model.view.MatchView
import com.aryanto.widi.footballapps.response.SearchMatchResponse
import com.aryanto.widi.footballapps.test.CoroutinesContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MatchSearchPresenter (
        private val view: MatchView,
        private val apiRepository: ApiRepository,
        private val gson: Gson,
        private val context: CoroutinesContextProvider = CoroutinesContextProvider()){

    fun getMatchList(team: String?){
        view.showLoading()

        GlobalScope.launch(context.main){

            val data = gson.fromJson(apiRepository.doRequest(TheSportDBApi.getMatchSearch(team)).await(), SearchMatchResponse::class.java)

            view.showMatch(data.event)
            view.hideLoading()
        }
    }
}