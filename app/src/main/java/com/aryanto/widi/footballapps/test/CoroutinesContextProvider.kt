package com.aryanto.widi.footballapps.test

import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext

open class CoroutinesContextProvider {
    open val main: CoroutineContext by lazy { Dispatchers.Main }
}