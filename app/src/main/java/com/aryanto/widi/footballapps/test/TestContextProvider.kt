package com.aryanto.widi.footballapps.test

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.coroutines.CoroutineContext

class TestContextProvider : CoroutinesContextProvider() {

    @ExperimentalCoroutinesApi
    override val main: CoroutineContext = Dispatchers.Unconfined
}