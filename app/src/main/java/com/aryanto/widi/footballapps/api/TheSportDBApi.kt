package com.aryanto.widi.footballapps.api

import com.aryanto.widi.footballapps.BuildConfig

object TheSportDBApi {

    fun getNextMatch(league: String?): String {
        return BuildConfig.NEXT_MATCH + league
    }

    fun getPastMatch(league: String?): String {
        return BuildConfig.PAST_MATCH + league
    }

    fun getDetail(matchId: String?): String {
        return BuildConfig.DETAIL_MATCH + matchId
    }

    fun getLeague(league: String?): String {
        return BuildConfig.TEAM_ALL + league
    }

    fun getTeam(teamId: String?): String {
        return BuildConfig.TEAM_URL + teamId
    }

    fun getPlayer(teamId: String?): String {
        return  BuildConfig.PLAYER_URL + teamId
    }

    fun getDetailPlayer(playerId: String?) : String {
        return BuildConfig.DETAIL_PLAYER + playerId
    }

    fun getMatchSearch(team: String?): String {
        return BuildConfig.SEARCH_MATCH + team
    }

    fun getTeamSearch(team: String?): String {
        return BuildConfig.SEARCH_TEAM + team
    }
}