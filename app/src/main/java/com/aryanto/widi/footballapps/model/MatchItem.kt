package com.aryanto.widi.footballapps.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MatchItem (

        @SerializedName("idEvent") var eventId: String? = null,
        @SerializedName("idHomeTeam") var homeTeamId: String? = null,
        @SerializedName("idAwayTeam") var awayTeamId: String? = null,

        @SerializedName("dateEvent") var dateMatch: String? = null,
        @SerializedName("strTime") var timeMatch: String? = null,
        @SerializedName("strHomeTeam") var teamHome: String? = null,
        @SerializedName("strAwayTeam") var teamAway: String? = null,
        @SerializedName("intHomeScore") var scoreHome: String? = null,
        @SerializedName("intAwayScore") var scoreAway: String? = null

) : Parcelable