package com.aryanto.widi.footballapps.fragment


import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.aryanto.widi.footballapps.R
import com.aryanto.widi.footballapps.fragment.favorite.MatchFavorite
import com.aryanto.widi.footballapps.fragment.favorite.TeamFavorite
import org.jetbrains.anko.find

/**
 * A simple [Fragment] subclass.
 *
 */
class FavoriteFragment : Fragment() {

    private lateinit var viewPager: ViewPager
    private lateinit var tabLayout: TabLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val views =  inflater.inflate(R.layout.fragment_favorite, container, false)

        viewPager = views.find(R.id.viewpager_favorite)
        viewPager.adapter = SliderAdapter(childFragmentManager)
        tabLayout = views.find(R.id.tab_favorite)
        tabLayout.post { tabLayout.setupWithViewPager(viewPager) }

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
        return views
    }

    inner class SliderAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        private var matches = "MATCHES"
        private var teams = "TEAMS"
        private val tabs = arrayOf(matches, teams)

        override fun getItem(position: Int): Fragment? {
            when (position) {
                0 -> return MatchFavorite()
                1 -> return TeamFavorite()
            }
            return null
        }

        override fun getCount(): Int {
            return tabs.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return tabs[position]
        }
    }
}