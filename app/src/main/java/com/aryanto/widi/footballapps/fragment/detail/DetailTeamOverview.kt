package com.aryanto.widi.footballapps.fragment.detail


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.aryanto.widi.footballapps.R

import com.aryanto.widi.footballapps.api.ApiRepository
import com.aryanto.widi.footballapps.model.TeamItem
import com.aryanto.widi.footballapps.model.view.details.DetailTeamView
import com.aryanto.widi.footballapps.presenter.details.DetailTeamPresenter
import com.google.gson.Gson
import org.jetbrains.anko.find

/**
 * A simple [Fragment] subclass.
 *
 */

class DetailTeamOverview : Fragment(), DetailTeamView {

    private lateinit var presenter: DetailTeamPresenter
    private lateinit var description: TextView
    private lateinit var id: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val views =  inflater.inflate(R.layout.fragment_detail_team_overview, container, false)
        description = views.find(R.id.description)

        id = arguments?.getString("id").toString()

        val request = ApiRepository()
        val gson = Gson()
        presenter = DetailTeamPresenter(this, request, gson)
        presenter.getDetailTeam(id)

        return views
    }

    override fun showLoading() {
    }

    override fun hideLoading() {
    }

    override fun showDetailTeam(data: List<TeamItem>) {
        description.text = data[0].teamDescription
    }

    companion object {
        fun newInstance(id: String) : DetailTeamOverview {
            val overviewFragment = DetailTeamOverview()
            val args = Bundle()
            args.putString("id", id)
            overviewFragment.arguments = args
            return overviewFragment
        }
    }
}
