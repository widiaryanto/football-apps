package com.aryanto.widi.footballapps.presenter

import com.aryanto.widi.footballapps.api.ApiRepository
import com.aryanto.widi.footballapps.api.TheSportDBApi
import com.aryanto.widi.footballapps.model.view.TeamView
import com.aryanto.widi.footballapps.response.TeamResponse
import com.aryanto.widi.footballapps.test.CoroutinesContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class TeamsPresenter (
        private val view: TeamView,
        private val apiRepository: ApiRepository,
        private val gson: Gson,
        private val context: CoroutinesContextProvider = CoroutinesContextProvider()){

    fun getTeamList(league: String?){
        view.showLoading()

        GlobalScope.launch(context.main){

            val data = gson.fromJson(apiRepository.doRequest(TheSportDBApi.getLeague(league)).await(), TeamResponse::class.java)

            view.showTeamList(data.teams)
            view.hideLoading()
        }
    }
}