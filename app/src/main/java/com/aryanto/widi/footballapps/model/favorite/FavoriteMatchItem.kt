package com.aryanto.widi.footballapps.model.favorite

data class FavoriteMatchItem (val id: Long?, val matchId: String?,
                              val matchDate: String?, val matchTime: String?,
                              // Home Team
                              val homeId: String?, val homeTeam: String?,
                              val imageHome: String?, val managerHome: String?,
                              val homeScore: String?, val homeForm: String?,
                              val homeGoal: String?, val homeShots: String?,
                              val homeRed: String?, val homeYellow: String?,
                              val homeGk: String?, val homeDef: String?,
                              val homeMid: String?, val homeForwd: String?,
                              val homeSub: String?,
                              // Away Team
                              val awayId: String?, val awayTeam: String?,
                              val imageAway: String?, val managerAway: String?,
                              val awayScore: String?, val awayForm: String?,
                              val awayGoal: String?, val awayShots: String?,
                              val awayRed: String?, val awayYellow: String?,
                              val awayGk: String?, val awayDef: String?,
                              val awayMid: String?, val awayForwd: String?,
                              val awaySub: String?){

    companion object {
        const val TABLE_MATCH_FAVORITE: String = "TABLE_MATCH_FAVORITE"
        const val ID: String = "ID_"
        const val MATCH_ID: String = "MATCH_ID"
        const val MATCH_DATE: String = "MATCH_DATE"
        const val MATCH_TIME: String = "MATCH_TIME"
        // Home Team
        const val HOME_ID: String = "HOME_ID"
        const val HOME_TEAM: String = "HOME_TEAM"
        const val IMAGE_HOME: String = "IMAGE_HOME"
        const val MANAGER_HOME: String = "MANAGER_HOME"
        const val HOME_SCORE: String = "HOME_SCORE"
        const val HOME_FORM: String = "HOME_FORM"
        const val HOME_GOAL: String = "HOME_GOAL"
        const val HOME_SHOTS: String = "HOME_SHOTS"
        const val HOME_RED: String = "HOME_RED"
        const val HOME_YELLOW: String = "HOME_YELLOW"
        const val HOME_GK: String = "HOME_GK"
        const val HOME_DEF: String = "HOME_DEF"
        const val HOME_MID: String = "HOME_MID"
        const val HOME_FORWD: String = "HOME_FORWD"
        const val HOME_SUB: String = "HOME_SUB"
        //Away Team
        const val AWAY_ID: String = "AWAY_ID"
        const val AWAY_TEAM: String = "AWAY_TEAM"
        const val IMAGE_AWAY: String = "IMAGE_AWAY"
        const val MANAGER_AWAY: String = "MANAGER_AWAY"
        const val AWAY_SCORE: String = "AWAY_SCORE"
        const val AWAY_FORM: String = "AWAY_FORM"
        const val AWAY_GOAL: String = "AWAY_GOAL"
        const val AWAY_SHOTS: String = "AWAY_SHOTS"
        const val AWAY_RED: String = "AWAY_RED"
        const val AWAY_YELLOW: String = "AWAY_YELLOW"
        const val AWAY_GK: String = "AWAY_GK"
        const val AWAY_DEF: String = "AWAY_DEF"
        const val AWAY_MID: String = "AWAY_MID"
        const val AWAY_FORWD: String = "AWAY_FORWD"
        const val AWAY_SUB: String = "AWAY_SUB"
    }
}