package com.aryanto.widi.footballapps.detail

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import com.aryanto.widi.footballapps.R
import com.aryanto.widi.footballapps.api.ApiRepository
import com.aryanto.widi.footballapps.model.PlayerItem
import com.aryanto.widi.footballapps.model.view.PlayerView
import com.aryanto.widi.footballapps.presenter.PlayerPresenter
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.ads.MobileAds
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import org.jetbrains.anko.find

class DetailPlayerActivity : AppCompatActivity(), PlayerView {

    private lateinit var presenter: PlayerPresenter
    private lateinit var pbDetailPlayer: ProgressBar
    private lateinit var layoutPlayer: LinearLayout
    private lateinit var playerName: TextView
    private lateinit var playerPhoto: ImageView
    private lateinit var playerHeight: TextView
    private lateinit var playerWeight: TextView
    private lateinit var playerPosition: TextView
    private lateinit var playerDesc: TextView
    private lateinit var playerId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_player)
        supportActionBar?.hide()

        pbDetailPlayer = find(R.id.pb_detail_player)
        layoutPlayer = find(R.id.layout_player)
        playerPhoto = find(R.id.image_detail_player)
        playerName = find(R.id.player_name)
        playerHeight = find(R.id.player_height)
        playerWeight = find(R.id.player_weight)
        playerPosition = find(R.id.player_position)
        playerDesc = find(R.id.player_desc)

        val intent = intent
        playerId = intent.getStringExtra("playerId")

        presenter = PlayerPresenter(this, ApiRepository(), Gson())
        presenter.getDetailPlayer(playerId)
    }

    override fun showLoading() {
        pbDetailPlayer.visibility = View.VISIBLE
        layoutPlayer.visibility = View.INVISIBLE
    }

    override fun hideLoading() {
        pbDetailPlayer.visibility = View.INVISIBLE
        layoutPlayer.visibility = View.VISIBLE
    }

    override fun showPlayer(data: List<PlayerItem>?) {
        playerName.text = data?.get(0)?.playerName
        Picasso.get().load(data?.get(0)?.playerPhoto).into(playerPhoto)
        playerHeight.text = data?.get(0)?.playerHeight
        playerWeight.text = data?.get(0)?.playerWeight
        playerPosition.text = data?.get(0)?.playerPosition
        playerDesc.text = data?.get(0)?.playerdesc
    }
}
