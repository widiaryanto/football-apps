package com.aryanto.widi.footballapps.fragment


import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.support.v7.widget.SearchView
import android.widget.LinearLayout

import com.aryanto.widi.footballapps.R
import com.aryanto.widi.footballapps.R.color.colorAccent
import com.aryanto.widi.footballapps.adapter.SearchAdapter
import com.aryanto.widi.footballapps.api.ApiRepository
import com.aryanto.widi.footballapps.detail.DetailMatchActivity
import com.aryanto.widi.footballapps.fragment.match.NextMatch
import com.aryanto.widi.footballapps.fragment.match.PastMatch
import com.aryanto.widi.footballapps.model.MatchItem
import com.aryanto.widi.footballapps.model.view.MatchView
import com.aryanto.widi.footballapps.presenter.search.MatchSearchPresenter
import com.google.gson.Gson
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.onRefresh

/**
 * A simple [Fragment] subclass.
 *
 */

class MatchFragment : Fragment(), MatchView {

    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var searchAdapter: SearchAdapter
    private lateinit var matchSearchPresenter: MatchSearchPresenter
    private lateinit var layoutMatch: LinearLayout
    private lateinit var rvMatch: RecyclerView
    private lateinit var viewPager: ViewPager
    private lateinit var tabLayout: TabLayout

    private val matchItem: MutableList<MatchItem> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_match, container, false)

    }

    override fun onViewCreated(views: View, savedInstanceState: Bundle?) {
        super.onViewCreated(views, savedInstanceState)

        swipeRefresh = views.find(R.id.refresh_match)
        swipeRefresh.setColorSchemeResources(colorAccent)

        layoutMatch = views.find(R.id.layout_match)
        rvMatch = views.find(R.id.rv_match)

        viewPager = views.find(R.id.viewpager_match)
        viewPager.adapter = SliderAdapter(childFragmentManager)

        tabLayout = views.find(R.id.tab_match)
        tabLayout.post { tabLayout.setupWithViewPager(viewPager) }

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

        swipeRefresh.visibility = View.GONE
        swipeRefresh.onRefresh {
            swipeRefresh.visibility = View.GONE
            layoutMatch.visibility = View.VISIBLE
        }
    }

    class SliderAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        private var next = "NEXT"
        private var past = "PAST"
        private val tabs = arrayOf(next, past)

        override fun getItem(position: Int): Fragment? {
            when (position) {
                0 -> return NextMatch()
                1 -> return PastMatch()
            }
            return null
        }

        override fun getCount(): Int {
            return tabs.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return tabs[position]
        }
    }

    override fun showLoading() {
    }

    override fun hideLoading() {
    }

    override fun showMatch(data: List<MatchItem>?) {
        swipeRefresh.isRefreshing = false
        data?.let {
            searchAdapter.refresh(it)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.searching, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        super.onPrepareOptionsMenu(menu)

        val searchMenuItem = menu?.findItem(R.id.search)
        val searchView = searchMenuItem?.actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                swipeRefresh.visibility = View.VISIBLE
                layoutMatch.visibility = View.GONE
                loadData()
                matchSearchPresenter.getMatchList(newText)
                return true
            }
        })
    }

    private fun loadData(){
        searchAdapter = SearchAdapter(context, matchItem){
            context?.startActivity<DetailMatchActivity>(
                    "idHome" to it.homeTeamId, "idAway" to it.awayTeamId,
                    "goalHome" to "", "goalAway" to "",
                    "homeTeam" to it.teamHome, "awayTeam" to it.teamAway,
                    "dateMatch" to it.dateMatch, "matchId" to it.eventId,
                    "timeMatch" to it.timeMatch)
        }
        rvMatch.layoutManager = LinearLayoutManager(context)
        rvMatch.adapter = searchAdapter

        val request = ApiRepository()
        val gson = Gson()
        matchSearchPresenter = MatchSearchPresenter(this, request, gson)
    }
}
