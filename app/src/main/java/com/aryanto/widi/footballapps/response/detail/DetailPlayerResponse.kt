package com.aryanto.widi.footballapps.response.detail

import com.aryanto.widi.footballapps.model.PlayerItem
import com.google.gson.annotations.SerializedName

data class DetailPlayerResponse (
        @SerializedName("players") val players: List<PlayerItem>)