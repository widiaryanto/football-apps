package com.aryanto.widi.footballapps.response.detail

import com.aryanto.widi.footballapps.model.DetailMatchItem
import com.google.gson.annotations.SerializedName

data class DetailMatchResponse (
        @SerializedName("events") val detailMatch: List<DetailMatchItem>)