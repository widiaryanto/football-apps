package com.aryanto.widi.footballapps.presenter

import com.aryanto.widi.footballapps.api.ApiRepository
import com.aryanto.widi.footballapps.api.TheSportDBApi
import com.aryanto.widi.footballapps.model.view.MatchView
import com.aryanto.widi.footballapps.response.MatchResponse
import com.aryanto.widi.footballapps.test.CoroutinesContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MatchPresenter (
        private val view: MatchView,
        private val apiRepository: ApiRepository,
        private val gson: Gson,
        private val context: CoroutinesContextProvider = CoroutinesContextProvider()){

    fun getNextMatch(league: String?){
        view.showLoading()

        GlobalScope.launch(context.main){

            val data = gson.fromJson(apiRepository.doRequest(TheSportDBApi.getNextMatch(league)).await(), MatchResponse::class.java)

            view.showMatch(data.matches)
            view.hideLoading()
        }
    }

    fun getPastMatch(league: String?){
        view.showLoading()

        GlobalScope.launch(context.main){

            val data = gson.fromJson(apiRepository.doRequest(TheSportDBApi.getPastMatch(league)).await(), MatchResponse::class.java)

            view.showMatch(data.matches)
            view.hideLoading()
        }
    }
}