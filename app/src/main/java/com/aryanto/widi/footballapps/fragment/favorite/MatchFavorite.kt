package com.aryanto.widi.footballapps.fragment.favorite


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aryanto.widi.footballapps.detail.DetailMatchActivity

import com.aryanto.widi.footballapps.R
import com.aryanto.widi.footballapps.R.color.colorAccent
import com.aryanto.widi.footballapps.adapter.favorite.FavoriteMatchAdapter
import com.aryanto.widi.footballapps.helper.database
import com.aryanto.widi.footballapps.model.favorite.FavoriteMatchItem
import com.aryanto.widi.footballapps.model.view.FavoriteView
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.onRefresh

/**
 * A simple [Fragment] subclass.
 *
 */

class MatchFavorite : Fragment(), FavoriteView {

    private lateinit var favoriteMatchAdapter: FavoriteMatchAdapter
    private lateinit var rvMatchFavorite: RecyclerView
    private lateinit var srMatchFavorite: SwipeRefreshLayout

    private var matchFavorite: MutableList<FavoriteMatchItem> = mutableListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val views = inflater.inflate(R.layout.fragment_match_favorite, container, false)

        rvMatchFavorite = views.find(R.id.rv_match_favorite)
        srMatchFavorite = views.find(R.id.sr_match_favorite)
        srMatchFavorite.setColorSchemeResources(colorAccent)

        views.let {
            favoriteMatchAdapter = FavoriteMatchAdapter(context, matchFavorite) {
                context?.startActivity<DetailMatchActivity>(
                        "idHome" to it.homeId, "idAway" to it.awayId,
                        "goalHome" to it.homeScore, "goalAway" to it.awayScore,
                        "homeTeam" to it.homeTeam, "awayTeam" to it.awayTeam,
                        "dateMatch" to it.matchDate, "matchId" to it.matchId,
                        "timeMatch" to it.matchTime)
            }
            rvMatchFavorite.layoutManager = LinearLayoutManager(context)
            rvMatchFavorite.adapter = favoriteMatchAdapter

            showFavorite()
            srMatchFavorite.onRefresh {
                matchFavorite.clear()
                showFavorite()
            }
        }
        return views
    }

    override fun showFavorite() {
        context?.database?.use {
            srMatchFavorite.isRefreshing = false
            val query = select(FavoriteMatchItem.TABLE_MATCH_FAVORITE)
            val favorites = query.parseList(classParser<FavoriteMatchItem>())
            matchFavorite.addAll(favorites)
            favoriteMatchAdapter.notifyDataSetChanged()
        }
    }
}
