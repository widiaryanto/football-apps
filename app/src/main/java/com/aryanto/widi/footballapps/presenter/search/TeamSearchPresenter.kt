package com.aryanto.widi.footballapps.presenter.search

import com.aryanto.widi.footballapps.api.ApiRepository
import com.aryanto.widi.footballapps.api.TheSportDBApi
import com.aryanto.widi.footballapps.model.view.TeamView
import com.aryanto.widi.footballapps.response.TeamResponse
import com.aryanto.widi.footballapps.test.CoroutinesContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class TeamSearchPresenter (
        private val view: TeamView,
        private val apiRepository: ApiRepository,
        private val gson: Gson,
        private val context: CoroutinesContextProvider = CoroutinesContextProvider()) {

    fun getTeamList(team: String?){
        view.showLoading()

        GlobalScope.launch(context.main){

            val data = gson.fromJson(apiRepository.doRequest(TheSportDBApi.getTeamSearch(team)).await(), TeamResponse::class.java)

            view.showTeamList(data.teams)
            view.hideLoading()
        }
    }
}