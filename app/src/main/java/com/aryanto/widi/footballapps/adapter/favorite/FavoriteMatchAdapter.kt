package com.aryanto.widi.footballapps.adapter.favorite

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.provider.CalendarContract
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aryanto.widi.footballapps.R
import com.aryanto.widi.footballapps.model.favorite.FavoriteMatchItem
import org.jetbrains.anko.find
import java.text.SimpleDateFormat
import java.util.*

class FavoriteMatchAdapter (private val context: Context?, private var favoriteMatch: List<FavoriteMatchItem>, private val listener: (FavoriteMatchItem) -> Unit)
    : RecyclerView.Adapter<FavoriteMatchAdapter.FavoriteMatchViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteMatchViewHolder {
        return FavoriteMatchViewHolder(LayoutInflater.from(context).inflate(R.layout.item_match, parent, false))
    }

    override fun onBindViewHolder(holder: FavoriteMatchViewHolder, position: Int) {
        holder.bindItem(favoriteMatch[position], listener)
    }

    override fun getItemCount(): Int {
        return favoriteMatch.size
    }

    class FavoriteMatchViewHolder (view: View) : RecyclerView.ViewHolder(view){
        private val dateMatch: TextView = view.find(R.id.date_match)
        private val timeMatch: TextView = view.find(R.id.time_match)
        private val homeTeam: TextView = view.find(R.id.home_team)
        private val awayTeam: TextView = view.find(R.id.away_team)
        private val scoreTeam: TextView = view.find(R.id.score_team)
        private val reminder: ImageView = view.find(R.id.reminder)

        @SuppressLint("SetTextI18n", "SimpleDateFormat")
        fun bindItem(favoriteMatch: FavoriteMatchItem, listener: (FavoriteMatchItem) -> Unit){

            val dateFinal = (favoriteMatch.matchDate+' '+favoriteMatch.matchTime).toDate().formatTo("EEEE, dd MMMM yyyy")
            val timeFinal = (favoriteMatch.matchDate+' '+favoriteMatch.matchTime).toDate().formatTo("HH:mm")

            dateMatch.text = dateFinal
            timeMatch.text = timeFinal
            homeTeam.text = favoriteMatch.homeTeam
            awayTeam.text = favoriteMatch.awayTeam

            reminder.setOnClickListener {
                val intent = Intent(Intent.ACTION_EDIT)
                val event = favoriteMatch.homeTeam + " vs " + favoriteMatch.awayTeam
                intent.type = "vnd.android.cursor.item/event"
                intent.putExtra(CalendarContract.Events.TITLE, event)
                intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, favoriteMatch.matchDate?.let {
                    it1 -> favoriteMatch.matchTime?.let {
                    it2 -> toGMTFormat(it1, it2)?.time }
                })
                intent.putExtra(CalendarContract.CalendarAlerts.ALARM_TIME, favoriteMatch.matchDate?.let {
                    it1 -> favoriteMatch.matchTime?.let {
                    it2 -> toGMTFormat(it1, it2)?.time }
                })
                itemView.context.startActivity(intent)
            }

            if (favoriteMatch.homeScore != ""){
                scoreTeam.text = favoriteMatch.homeScore + " vs " + favoriteMatch.awayScore
                reminder.visibility = View.INVISIBLE
            } else {
                scoreTeam.text = " vs "
                reminder.visibility = View.VISIBLE
            }

            itemView.setOnClickListener {
                listener(favoriteMatch)
            }
        }

        @SuppressLint("SimpleDateFormat")
        fun String.toDate(dateFormat: String = "yyyy-MM-dd HH:mm:ssXXX", timeZone: TimeZone = TimeZone.getTimeZone("UTC")): Date {
            val parser = SimpleDateFormat(dateFormat, Locale.getDefault())
            parser.timeZone = timeZone
            return parser.parse(this)
        }

        fun Date.formatTo(dateFormat: String, timeZone: TimeZone = TimeZone.getDefault()): String {
            val formatter = SimpleDateFormat(dateFormat, Locale.getDefault())
            formatter.timeZone = timeZone
            return formatter.format(this)
        }

        @SuppressLint("SimpleDateFormat")
        fun toGMTFormat(date: String, time: String): Date? {
            val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm")
            formatter.timeZone = TimeZone.getTimeZone("UTC")
            val dateTime = "$date $time"
            return formatter.parse(dateTime)
        }
    }
}