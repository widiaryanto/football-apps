package com.aryanto.widi.footballapps.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aryanto.widi.footballapps.R
import com.aryanto.widi.footballapps.model.PlayerItem
import com.squareup.picasso.Picasso
import org.jetbrains.anko.find

class PlayerAdapter (private val context: Context?, private var playerItem: List<PlayerItem>, private var listener: (PlayerItem) -> Unit)
    : RecyclerView.Adapter<PlayerAdapter.PlayerViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayerViewHolder {
        return PlayerViewHolder(LayoutInflater.from(context).inflate(R.layout.item_player, parent, false))
    }

    override fun onBindViewHolder(holder: PlayerViewHolder, position: Int) {
        holder.bindItem(playerItem[position], listener)
    }

    override fun getItemCount(): Int {
        return playerItem.size
    }

    fun refresh(fill: List<PlayerItem>){
        playerItem = fill
        notifyDataSetChanged()
    }

    class PlayerViewHolder(view: View) : RecyclerView.ViewHolder(view){
        private val playerPhoto: ImageView = view.find(R.id.player_photo)
        private val playerName: TextView = view.find(R.id.player_name)
        private val playerPosition: TextView = view.find(R.id.player_position)

        fun bindItem(playerItem: PlayerItem, listener: (PlayerItem) -> Unit){
            Picasso.get().load(playerItem.playerPhoto).into(playerPhoto)
            playerName.text = playerItem.playerName
            playerPosition.text = playerItem.playerPosition

            itemView.setOnClickListener {
                listener(playerItem)
            }
        }
    }
}