package com.aryanto.widi.footballapps.fragment.match


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ProgressBar
import android.widget.Spinner
import com.aryanto.widi.footballapps.detail.DetailMatchActivity

import com.aryanto.widi.footballapps.R
import com.aryanto.widi.footballapps.adapter.MatchAdapter
import com.aryanto.widi.footballapps.api.ApiRepository
import com.aryanto.widi.footballapps.model.MatchItem
import com.aryanto.widi.footballapps.model.view.MatchView
import com.aryanto.widi.footballapps.presenter.MatchPresenter
import com.google.gson.Gson
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.onRefresh

/**
 * A simple [Fragment] subclass.
 *
 */

class NextMatch : Fragment(), MatchView {

    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var spinnerNext: Spinner
    private lateinit var matchAdapter: MatchAdapter
    private lateinit var matchPresenter: MatchPresenter
    private lateinit var pbNext: ProgressBar
    private lateinit var rvNext: RecyclerView
    private lateinit var idLeague: String

    private val matchItem: MutableList<MatchItem> = mutableListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val views =  inflater.inflate(R.layout.fragment_next_match, container, false)
        setHasOptionsMenu(true)

        swipeRefresh = views.find(R.id.refresh_next)
        spinnerNext = views.find(R.id.spinner_next)
        pbNext = views.find(R.id.pb_next)
        rvNext = views.find(R.id.rv_next)

        views.let {
            matchAdapter = MatchAdapter(context, matchItem){
                context?.startActivity<DetailMatchActivity>(
                        "idHome" to it.homeTeamId, "idAway" to it.awayTeamId,
                        "goalHome" to "", "goalAway" to "",
                        "homeTeam" to it.teamHome, "awayTeam" to it.teamAway,
                        "dateMatch" to it.dateMatch, "matchId" to it.eventId,
                        "timeMatch" to it.timeMatch)
            }
            rvNext.layoutManager = LinearLayoutManager(context)
            rvNext.adapter = matchAdapter

            val request = ApiRepository()
            val gson = Gson()
            matchPresenter = MatchPresenter(this, request, gson)
        }

        val spinnerItems = resources.getStringArray(R.array.league)
        val spinnerAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, spinnerItems)
        spinnerNext.adapter = spinnerAdapter

        spinnerNext.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                when (p2) {
                    0 -> idLeague = "4328"
                    1 -> idLeague = "4335"
                    2 -> idLeague = "4331"
                    3 -> idLeague = "4332"
                    4 -> idLeague = "4334"
                }
                matchPresenter.getNextMatch(idLeague)
            }
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }
        }
        swipeRefresh.onRefresh {
            matchPresenter.getNextMatch(idLeague)
        }

        return views
    }

    override fun showLoading() {
        pbNext.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        pbNext.visibility = View.INVISIBLE
    }

    override fun showMatch(data: List<MatchItem>?) {
        swipeRefresh.isRefreshing = false
        data?.let {
            matchAdapter.refresh(it)
        }
    }
}
