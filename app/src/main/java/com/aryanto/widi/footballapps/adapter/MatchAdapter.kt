package com.aryanto.widi.footballapps.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.provider.CalendarContract
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aryanto.widi.footballapps.R
import com.aryanto.widi.footballapps.model.MatchItem
import org.jetbrains.anko.find
import java.text.SimpleDateFormat
import java.util.*

class MatchAdapter (private val context: Context?, private var matchItem: List<MatchItem>, private var listener: (MatchItem) -> Unit)
    : RecyclerView.Adapter<MatchAdapter.MatchViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MatchViewHolder {
        return MatchViewHolder(LayoutInflater.from(context).inflate(R.layout.item_match, parent, false))
    }

    override fun onBindViewHolder(holder: MatchViewHolder, position: Int) {
        holder.bindItem(matchItem[position], listener)
    }

    override fun getItemCount(): Int {
        return matchItem.size
    }

    fun refresh(fill: List<MatchItem>){
        matchItem = fill
        notifyDataSetChanged()
    }

    class MatchViewHolder(view: View) : RecyclerView.ViewHolder(view){
        private val dateMatch: TextView = view.find(R.id.date_match)
        private val timeMatch: TextView = view.find(R.id.time_match)
        private val homeTeam: TextView = view.find(R.id.home_team)
        private val awayTeam: TextView = view.find(R.id.away_team)
        private val scoreTeam: TextView = view.find(R.id.score_team)
        private val reminder: ImageView = view.find(R.id.reminder)

        @SuppressLint("SetTextI18n", "SimpleDateFormat")
        fun bindItem(matchItem: MatchItem, listener: (MatchItem) -> Unit) {

            val dateFinal = (matchItem.dateMatch+' '+matchItem.timeMatch).toDate().formatTo("EEEE, dd MMMM yyyy")
            val timeFinal = (matchItem.dateMatch+' '+matchItem.timeMatch).toDate().formatTo("HH:mm")

            dateMatch.text = dateFinal
            timeMatch.text = timeFinal
            homeTeam.text = matchItem.teamHome
            awayTeam.text = matchItem.teamAway

            reminder.setOnClickListener {
                val intent = Intent(Intent.ACTION_EDIT)
                val event = matchItem.teamHome + " vs " + matchItem.teamAway
                intent.type = "vnd.android.cursor.item/event"
                intent.putExtra(CalendarContract.Events.TITLE, event)
                intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, matchItem.dateMatch?.let {
                    it1 -> matchItem.timeMatch?.let {
                    it2 -> toGMTFormat(it1, it2)?.time }
                })
                intent.putExtra(CalendarContract.CalendarAlerts.ALARM_TIME, matchItem.dateMatch?.let {
                    it1 -> matchItem.timeMatch?.let {
                    it2 -> toGMTFormat(it1, it2)?.time }
                })
                itemView.context.startActivity(intent)
            }

            if (matchItem.scoreHome != null){
                scoreTeam.text = matchItem.scoreHome + " vs " + matchItem.scoreAway
                reminder.visibility = View.INVISIBLE
            } else {
                scoreTeam.text = " vs "
                reminder.visibility = View.VISIBLE
            }

            itemView.setOnClickListener {
                listener(matchItem)
            }
        }

        @SuppressLint("SimpleDateFormat")
        fun String.toDate(dateFormat: String = "yyyy-MM-dd HH:mm:ss", timeZone: TimeZone = TimeZone.getTimeZone("UTC")): Date {
            val parser = SimpleDateFormat(dateFormat, Locale.getDefault())
            parser.timeZone = timeZone
            return parser.parse(this)
        }

        fun Date.formatTo(dateFormat: String, timeZone: TimeZone = TimeZone.getDefault()): String {
            val formatter = SimpleDateFormat(dateFormat, Locale.getDefault())
            formatter.timeZone = timeZone
            return formatter.format(this)
        }

        @SuppressLint("SimpleDateFormat")
        fun toGMTFormat(date: String, time: String): Date? {
            val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm")
            formatter.timeZone = TimeZone.getTimeZone("UTC")
            val dateTime = "$date $time"
            return formatter.parse(dateTime)
        }
    }
}