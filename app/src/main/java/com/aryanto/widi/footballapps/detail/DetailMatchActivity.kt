package com.aryanto.widi.footballapps.detail

import android.annotation.SuppressLint
import android.database.sqlite.SQLiteConstraintException
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.ScrollView
import com.aryanto.widi.footballapps.R
import com.aryanto.widi.footballapps.R.id.add_to_favorite
import com.aryanto.widi.footballapps.api.ApiRepository
import com.aryanto.widi.footballapps.helper.database
import com.aryanto.widi.footballapps.model.DetailMatchItem
import com.aryanto.widi.footballapps.model.favorite.FavoriteMatchItem
import com.aryanto.widi.footballapps.model.MatchItem
import com.aryanto.widi.footballapps.model.TeamItem
import com.aryanto.widi.footballapps.model.view.details.DetailMatchView
import com.aryanto.widi.footballapps.presenter.details.DetailMatchPresenter
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.ads.MobileAds
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_match.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.find
import org.jetbrains.anko.toast
import java.text.SimpleDateFormat
import java.util.*

class DetailMatchActivity : AppCompatActivity(), DetailMatchView {

    private lateinit var detailMatchPresenter: DetailMatchPresenter
    private lateinit var detailMatches: DetailMatchItem
    private lateinit var homes: TeamItem
    private lateinit var aways: TeamItem
    private lateinit var matches: MatchItem
    private lateinit var matchId: String
    private lateinit var pbMatch: ProgressBar
    private lateinit var svMatch: ScrollView

    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false

    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_match)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Match Detail"

        pbMatch = find(R.id.pb_detail_match)
        svMatch = find(R.id.sv_detail_match)

        val intent = intent
        matchId = intent.getStringExtra("matchId")

        baseContext.let {
            detailMatchPresenter = DetailMatchPresenter(this, ApiRepository(), Gson())

            val homeId: String? = intent.getStringExtra("idHome")
            val awayId: String? = intent.getStringExtra("idAway")
            val dateMatch: String? = intent.getStringExtra("dateMatch")
            val timeMatch: String? = intent.getStringExtra("timeMatch")
            val homeTeam: String? = intent.getStringExtra("homeTeam")
            val awayTeam: String? = intent.getStringExtra("awayTeam")
            val scoreHome: String? = intent.getStringExtra("goalHome")
            val scoreAway: String? = intent.getStringExtra("goalAway")

            val dateFinal = ("$dateMatch $timeMatch").toDate().formatTo("EEEE, dd MMMM yyyy")
            val timeFinal = ("$dateMatch $timeMatch").toDate().formatTo("HH:mm")

            tv_date.text = dateFinal
            tv_time.text = timeFinal
            tv_team_home.text = homeTeam
            tv_team_away.text = awayTeam
            tv_score.text = "$scoreHome vs $scoreAway"

            favoriteState()
            detailMatchPresenter.loadDetailMatch(homeId, awayId, matchId)
            matches = MatchItem(matchId, homeId, awayId, dateMatch, timeMatch, homeTeam, awayTeam, scoreHome, scoreAway)
        }
    }

    override fun showLoading() {
        pbMatch.visibility = View.VISIBLE
        svMatch.visibility = View.INVISIBLE
    }

    override fun hideLoading() {
        pb_detail_match.visibility = View.INVISIBLE
        svMatch.visibility = View.VISIBLE
    }

    override fun showDetail(home: List<TeamItem>?, away: List<TeamItem>?, detail: List<DetailMatchItem>?) {
        homes = TeamItem(home?.get(0)?.teamBadge, home?.get(0)?.teamManager)
        aways = TeamItem(away?.get(0)?.teamBadge, away?.get(0)?.teamManager)

        val detailData = detail?.get(0)
        detailData?.run {
            detailMatches = DetailMatchItem (
                    //Home Team
                    homeForm, homeGoal, homeShots, homeRed,
                    homeYellow, homeGk, homeDef, homeMid,
                    homeForwd, homeSub,

                    //Away Team
                    awayForm, awayGoal, awayShots, awayRed,
                    awayYellow, awayGk, awayDef, awayMid,
                    awayForwd, awaySub
            )
        }

        //Home Team
        Picasso.get().load(home?.get(0)?.teamBadge).into(img_home)
        tv_manager_home.text = home?.get(0)?.teamManager
        tv_form_home.text = detail?.get(0)?.homeForm
        tv_goal_home.text = detail?.get(0)?.homeGoal
        tv_shots_home.text = detail?.get(0)?.homeShots
        tv_red_home.text = detail?.get(0)?.homeRed
        tv_yellow_home.text = detail?.get(0)?.homeYellow
        tv_gk_home.text = detail?.get(0)?.homeGk
        tv_def_home.text = detail?.get(0)?.homeDef
        tv_mid_home.text = detail?.get(0)?.homeMid
        tv_forwd_home.text = detail?.get(0)?.homeForwd
        tv_sub_home.text = detail?.get(0)?.homeSub

        //Away Team
        Picasso.get().load(away?.get(0)?.teamBadge).into(img_away)
        tv_manager_away.text = away?.get(0)?.teamManager
        tv_form_away.text = detail?.get(0)?.awayForm
        tv_goal_away.text = detail?.get(0)?.awayGoal
        tv_shots_away.text = detail?.get(0)?.awayShots
        tv_red_away.text = detail?.get(0)?.awayRed
        tv_yellow_away.text = detail?.get(0)?.awayYellow
        tv_gk_away.text = detail?.get(0)?.awayGk
        tv_def_away.text = detail?.get(0)?.awayDef
        tv_mid_away.text = detail?.get(0)?.awayMid
        tv_forwd_away.text = detail?.get(0)?.awayForwd
        tv_sub_away.text = detail?.get(0)?.awaySub
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.detail_menu, menu)
        menuItem = menu
        setFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when(item?.itemId){
            android.R.id.home -> {
                finish()
                true
            }
            add_to_favorite -> {
                if (isFavorite) removeFromFavorite() else addToFavorite()
                isFavorite =! isFavorite
                setFavorite()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun addToFavorite() {
        try {
            database.use {
                insert(FavoriteMatchItem.TABLE_MATCH_FAVORITE,
                        FavoriteMatchItem.MATCH_ID to matches.eventId, FavoriteMatchItem.MATCH_DATE to matches.dateMatch,
                        FavoriteMatchItem.MATCH_TIME to matches.timeMatch,
                        //Home Team
                        FavoriteMatchItem.HOME_ID to matches.homeTeamId, FavoriteMatchItem.HOME_TEAM to matches.teamHome,
                        FavoriteMatchItem.IMAGE_HOME to homes.teamBadge, FavoriteMatchItem.MANAGER_HOME to homes.teamManager,
                        FavoriteMatchItem.HOME_SCORE to matches.scoreHome, FavoriteMatchItem.HOME_FORM to detailMatches.homeForm,
                        FavoriteMatchItem.HOME_GOAL to detailMatches.homeGoal, FavoriteMatchItem.HOME_SHOTS to detailMatches.homeShots,
                        FavoriteMatchItem.HOME_RED to detailMatches.homeRed, FavoriteMatchItem.HOME_YELLOW to detailMatches.homeYellow,
                        FavoriteMatchItem.HOME_GK to detailMatches.homeGk, FavoriteMatchItem.HOME_DEF to detailMatches.homeDef,
                        FavoriteMatchItem.HOME_MID to detailMatches.homeMid, FavoriteMatchItem.HOME_FORWD to detailMatches.homeForwd,
                        FavoriteMatchItem.HOME_SUB to detailMatches.homeSub,
                        //Away Team
                        FavoriteMatchItem.AWAY_ID to matches.awayTeamId, FavoriteMatchItem.AWAY_TEAM to matches.teamAway,
                        FavoriteMatchItem.IMAGE_AWAY to aways.teamBadge, FavoriteMatchItem.MANAGER_AWAY to aways.teamManager,
                        FavoriteMatchItem.AWAY_SCORE to matches.scoreAway, FavoriteMatchItem.AWAY_FORM to detailMatches.awayForm,
                        FavoriteMatchItem.AWAY_GOAL to detailMatches.awayGoal, FavoriteMatchItem.AWAY_SHOTS to detailMatches.awayShots,
                        FavoriteMatchItem.AWAY_RED to detailMatches.awayRed, FavoriteMatchItem.AWAY_YELLOW to detailMatches.awayYellow,
                        FavoriteMatchItem.AWAY_GK to detailMatches.awayGk, FavoriteMatchItem.AWAY_DEF to detailMatches.awayDef,
                        FavoriteMatchItem.AWAY_MID to detailMatches.awayMid, FavoriteMatchItem.AWAY_FORWD to detailMatches.awayForwd,
                        FavoriteMatchItem.AWAY_SUB to detailMatches.awaySub)
            }
            toast("Added to favorite")
        } catch (e: SQLiteConstraintException) {
            toast(e.localizedMessage)
        }
    }

    override fun removeFromFavorite() {
        try {
            database.use {
                delete(FavoriteMatchItem.TABLE_MATCH_FAVORITE, "(MATCH_ID = {matchId})",
                "matchId" to matchId)
            }
            toast("Removed from favorite")
        } catch (e: SQLiteConstraintException){
            toast(e.localizedMessage)
        }
    }

    override fun favoriteState() {
        database.use {
            val result = select(FavoriteMatchItem.TABLE_MATCH_FAVORITE)
                    .whereArgs("(MATCH_ID = {matchId})",
                            "matchId" to matchId)
            val favorite = result.parseList(classParser<FavoriteMatchItem>())
            if (!favorite.isEmpty()) isFavorite = true
        }
    }

    override fun setFavorite() {
        if(isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_added_to_favorites)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_add_to_favorites)
    }

    @SuppressLint("SimpleDateFormat")
    fun String.toDate(dateFormat: String = "yyyy-MM-dd HH:mm:ss", timeZone: TimeZone = TimeZone.getTimeZone("UTC")): Date {
        val parser = SimpleDateFormat(dateFormat, Locale.getDefault())
        parser.timeZone = timeZone
        return parser.parse(this)
    }

    fun Date.formatTo(dateFormat: String, timeZone: TimeZone = TimeZone.getDefault()): String {
        val formatter = SimpleDateFormat(dateFormat, Locale.getDefault())
        formatter.timeZone = timeZone
        return formatter.format(this)
    }
}
