package com.aryanto.widi.footballapps.response

import com.aryanto.widi.footballapps.model.PlayerItem
import com.google.gson.annotations.SerializedName

data class PlayerResponse (
        @SerializedName("player") val player: List<PlayerItem>)