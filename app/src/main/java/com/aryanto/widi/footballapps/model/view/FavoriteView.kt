package com.aryanto.widi.footballapps.model.view

interface FavoriteView {
    fun showFavorite()
}