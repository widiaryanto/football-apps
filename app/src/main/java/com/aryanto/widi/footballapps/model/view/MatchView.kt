package com.aryanto.widi.footballapps.model.view

import com.aryanto.widi.footballapps.model.MatchItem

interface MatchView {
    fun showLoading()
    fun hideLoading()
    fun showMatch(data: List<MatchItem>?)
}