package com.aryanto.widi.footballapps.helper

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.aryanto.widi.footballapps.model.favorite.FavoriteMatchItem
import com.aryanto.widi.footballapps.model.favorite.FavoriteTeamItem
import org.jetbrains.anko.db.*

class MatchDatabaseOpenHelper (ctx: Context) : ManagedSQLiteOpenHelper(ctx, "Favorite.db", null, 1) {
    companion object {
        private var instance: MatchDatabaseOpenHelper? = null

        @Synchronized
        fun getInstance(ctx: Context) : MatchDatabaseOpenHelper {
            if (instance == null){
                instance = MatchDatabaseOpenHelper(ctx.applicationContext)
            }
            return instance as MatchDatabaseOpenHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.createTable(FavoriteMatchItem.TABLE_MATCH_FAVORITE, true,
                FavoriteMatchItem.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                FavoriteMatchItem.MATCH_ID to TEXT + UNIQUE, FavoriteMatchItem.MATCH_DATE to TEXT,
                FavoriteMatchItem.MATCH_TIME to TEXT,
                //Home Team
                FavoriteMatchItem.HOME_ID to TEXT, FavoriteMatchItem.HOME_TEAM to TEXT,
                FavoriteMatchItem.IMAGE_HOME to TEXT, FavoriteMatchItem.MANAGER_HOME to TEXT,
                FavoriteMatchItem.HOME_SCORE to TEXT, FavoriteMatchItem.HOME_FORM to TEXT,
                FavoriteMatchItem.HOME_GOAL to TEXT, FavoriteMatchItem.HOME_SHOTS to TEXT,
                FavoriteMatchItem.HOME_RED to TEXT, FavoriteMatchItem.HOME_YELLOW to TEXT,
                FavoriteMatchItem.HOME_GK to TEXT, FavoriteMatchItem.HOME_DEF to TEXT,
                FavoriteMatchItem.HOME_MID to TEXT, FavoriteMatchItem.HOME_FORWD to TEXT,
                FavoriteMatchItem.HOME_SUB to TEXT,
                //Away Team
                FavoriteMatchItem.AWAY_ID to TEXT, FavoriteMatchItem.AWAY_TEAM to TEXT,
                FavoriteMatchItem.IMAGE_AWAY to TEXT, FavoriteMatchItem.MANAGER_AWAY to TEXT,
                FavoriteMatchItem.AWAY_SCORE to TEXT, FavoriteMatchItem.AWAY_FORM to TEXT,
                FavoriteMatchItem.AWAY_GOAL to TEXT, FavoriteMatchItem.AWAY_SHOTS to TEXT,
                FavoriteMatchItem.AWAY_RED to TEXT, FavoriteMatchItem.AWAY_YELLOW to TEXT,
                FavoriteMatchItem.AWAY_GK to TEXT, FavoriteMatchItem.AWAY_DEF to TEXT,
                FavoriteMatchItem.AWAY_MID to TEXT, FavoriteMatchItem.AWAY_FORWD to TEXT,
                FavoriteMatchItem.AWAY_SUB to TEXT)

        db.createTable(FavoriteTeamItem.TABLE_TEAM_FAVORITE, true,
                FavoriteTeamItem.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                FavoriteTeamItem.TEAM_ID to TEXT + UNIQUE,
                FavoriteTeamItem.TEAM_NAME to TEXT,
                FavoriteTeamItem.TEAM_BADGE to TEXT)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.dropTable(FavoriteMatchItem.TABLE_MATCH_FAVORITE, true)
        db.dropTable(FavoriteTeamItem.TABLE_TEAM_FAVORITE, true)
    }
}

val Context.database: MatchDatabaseOpenHelper
get() = MatchDatabaseOpenHelper.getInstance(applicationContext)