package com.aryanto.widi.footballapps.api

import com.aryanto.widi.footballapps.BuildConfig
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import java.net.URL

class ApiRepository {

    fun doRequest(url: String): Deferred<String> = GlobalScope.async {
        URL(url).readText()
    }

    fun loadTeam(teamId: String?): Deferred<String> = GlobalScope.async {
        URL(BuildConfig.TEAM_URL + teamId).readText()
    }

    fun loadDetail(matchId: String?): Deferred<String> = GlobalScope.async {
        URL(BuildConfig.DETAIL_MATCH + matchId).readText()
    }
}