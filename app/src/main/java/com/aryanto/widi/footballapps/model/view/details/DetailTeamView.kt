package com.aryanto.widi.footballapps.model.view.details

import com.aryanto.widi.footballapps.model.TeamItem

interface DetailTeamView {
    fun showLoading()
    fun hideLoading()
    fun showDetailTeam(data: List<TeamItem>)
}