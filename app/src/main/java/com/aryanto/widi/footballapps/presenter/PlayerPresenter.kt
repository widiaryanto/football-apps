package com.aryanto.widi.footballapps.presenter

import com.aryanto.widi.footballapps.api.ApiRepository
import com.aryanto.widi.footballapps.api.TheSportDBApi
import com.aryanto.widi.footballapps.model.view.PlayerView
import com.aryanto.widi.footballapps.response.detail.DetailPlayerResponse
import com.aryanto.widi.footballapps.response.PlayerResponse
import com.aryanto.widi.footballapps.test.CoroutinesContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class PlayerPresenter (
        private val view: PlayerView,
        private val apiRepository: ApiRepository,
        private val gson: Gson,
        private val context: CoroutinesContextProvider = CoroutinesContextProvider()) {

    fun getPlayerList(teamId: String?){
        view.showLoading()

        GlobalScope.launch(context.main){

            val data = gson.fromJson(apiRepository.doRequest(TheSportDBApi.getPlayer(teamId)).await(), PlayerResponse::class.java)

            view.showPlayer(data.player)
            view.hideLoading()
        }
    }

    fun getDetailPlayer(playerId: String?){
        view.showLoading()

        GlobalScope.launch(context.main){

            val data = gson.fromJson(apiRepository.doRequest(TheSportDBApi.getDetailPlayer(playerId)).await(), DetailPlayerResponse::class.java)

            view.showPlayer(data.players)
            view.hideLoading()
        }
    }
}