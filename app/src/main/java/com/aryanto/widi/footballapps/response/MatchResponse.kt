package com.aryanto.widi.footballapps.response

import com.aryanto.widi.footballapps.model.MatchItem
import com.google.gson.annotations.SerializedName

data class MatchResponse (
        @SerializedName("events") val matches: List<MatchItem>)