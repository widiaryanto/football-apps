package com.aryanto.widi.footballapps.response

import com.aryanto.widi.footballapps.model.TeamItem
import com.google.gson.annotations.SerializedName

data class TeamResponse (
        @SerializedName("teams") val teams: List<TeamItem>)