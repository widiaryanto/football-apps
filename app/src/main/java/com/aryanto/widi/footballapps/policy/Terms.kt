package com.aryanto.widi.footballapps.policy

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import com.aryanto.widi.footballapps.R

class Terms : AppCompatActivity() {

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terms)

        val myWebView: WebView = findViewById(R.id.term_webview)
        myWebView.loadUrl("https://jadwaliga.com/#terms")
        myWebView.settings.javaScriptEnabled = true
    }
}
