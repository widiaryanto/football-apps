package com.aryanto.widi.footballapps.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PlayerItem (

        @SerializedName("idPlayer") var playerId: String? = null,
        @SerializedName("strPlayer") var playerName: String? = null,
        @SerializedName("strThumb") var playerPhoto: String? = null,
        @SerializedName("strPosition") var playerPosition: String? = null,
        @SerializedName("strHeight") var playerHeight: String? = null,
        @SerializedName("strWeight") var playerWeight: String? = null,
        @SerializedName("strDescriptionEN") var playerdesc: String? = null

) : Parcelable