package com.aryanto.widi.footballapps.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DetailMatchItem (

        //Home Team
        @SerializedName("strHomeFormation") val homeForm: String? = null,
        @SerializedName("strHomeGoalDetails") val homeGoal: String? = null,
        @SerializedName("intHomeShots") val homeShots: String? = null,
        @SerializedName("strHomeRedCards") val homeRed: String? = null,
        @SerializedName("strHomeYellowCards") val homeYellow: String? = null,
        @SerializedName("strHomeLineupGoalkeeper") val homeGk: String? = null,
        @SerializedName("strHomeLineupDefense") val homeDef: String? = null,
        @SerializedName("strHomeLineupMidfield") val homeMid: String? = null,
        @SerializedName("strHomeLineupForward") val homeForwd: String? = null,
        @SerializedName("strHomeLineupSubstitutes") val homeSub: String? = null,

        //Away Team
        @SerializedName("strAwayFormation") val awayForm: String? = null,
        @SerializedName("strAwayGoalDetails") val awayGoal: String? = null,
        @SerializedName("intAwayShots") val awayShots: String? = null,
        @SerializedName("strAwayRedCards") val awayRed: String? = null,
        @SerializedName("strAwayYellowCards") val awayYellow: String? = null,
        @SerializedName("strAwayLineupGoalkeeper") val awayGk: String? = null,
        @SerializedName("strAwayLineupDefense") val awayDef: String? = null,
        @SerializedName("strAwayLineupMidfield") val awayMid: String? = null,
        @SerializedName("strAwayLineupForward") val awayForwd: String? = null,
        @SerializedName("strAwayLineupSubstitutes") val awaySub: String? = null

) : Parcelable