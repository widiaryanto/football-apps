package com.aryanto.widi.footballapps.presenter.details

import com.aryanto.widi.footballapps.api.ApiRepository
import com.aryanto.widi.footballapps.api.TheSportDBApi
import com.aryanto.widi.footballapps.model.view.details.DetailTeamView
import com.aryanto.widi.footballapps.response.TeamResponse
import com.aryanto.widi.footballapps.test.CoroutinesContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class DetailTeamPresenter (
        private val view: DetailTeamView,
        private val apiRepository: ApiRepository,
        private val gson: Gson,
        private val context: CoroutinesContextProvider = CoroutinesContextProvider()) {

    fun getDetailTeam(teamId: String){
        view.showLoading()

        GlobalScope.launch(context.main) {

            val data = gson.fromJson(apiRepository.doRequest(TheSportDBApi.getTeam(teamId)).await(), TeamResponse::class.java)

            view.showDetailTeam(data.teams)
            view.hideLoading()
        }
    }
}