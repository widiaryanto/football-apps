package com.aryanto.widi.footballapps.fragment.detail


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aryanto.widi.footballapps.detail.DetailPlayerActivity

import com.aryanto.widi.footballapps.R
import com.aryanto.widi.footballapps.adapter.PlayerAdapter
import com.aryanto.widi.footballapps.api.ApiRepository
import com.aryanto.widi.footballapps.model.PlayerItem
import com.aryanto.widi.footballapps.model.view.PlayerView
import com.aryanto.widi.footballapps.presenter.PlayerPresenter
import com.google.gson.Gson
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivity

/**
 * A simple [Fragment] subclass.
 *
 */

class DetailTeamPlayer : Fragment(), PlayerView {

    private lateinit var presenter: PlayerPresenter
    private lateinit var playerAdapter: PlayerAdapter
    private lateinit var rvPlayer: RecyclerView
    private lateinit var id: String

    private val playerItem: MutableList<PlayerItem> = mutableListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val views =  inflater.inflate(R.layout.fragment_detail_team_player, container, false)

        id = arguments?.getString("id").toString()
        rvPlayer = views.find(R.id.rv_player)

        views.let {
            playerAdapter = PlayerAdapter(context, playerItem){
                context?.startActivity<DetailPlayerActivity>("playerId" to "${it.playerId}")
            }
            rvPlayer.layoutManager = LinearLayoutManager(context)
            rvPlayer.adapter = playerAdapter

            presenter = PlayerPresenter(this, ApiRepository(), Gson())
            presenter.getPlayerList(id)
        }
        return views
    }

    override fun showLoading() {
    }

    override fun hideLoading() {
    }

    override fun showPlayer(data: List<PlayerItem>?) {
        data?.let {
            playerAdapter.refresh(it)
        }
    }

    companion object {
        fun newInstance(id: String) : DetailTeamPlayer {
            val overviewFragment = DetailTeamPlayer()
            val args = Bundle()
            args.putString("id", id)
            overviewFragment.arguments = args
            return overviewFragment
        }
    }
}
